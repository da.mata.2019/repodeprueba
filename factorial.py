def factorial(num):
    if num == 0 or num == 1:
        resultado = 1
    else:
        resultado = num * factorial(num - 1)
    return resultado


for i in range(1, 11):
    print('El factorial de {} es: {}'.format(i, factorial(i)))

